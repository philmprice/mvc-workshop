<?php

//  bootstrap
include "../app/config/bootstrap.php";
include "../app/config/routes.php";
    
try {

    //  create app
    $application = new \Phalcon\Mvc\Application($di);

    //  handle request
    echo $application->handle()->getContent();

} catch (\Phalcon\Exception $e) {

    //  show exception
    echo $e->getMessage();
}
