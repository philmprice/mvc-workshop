<?php

/////////////////////////////////////////////////////////////////////////////
//  error reporting

error_reporting(E_ALL);
ini_set('display_errors', 1);

/////////////////////////////////////////////////////////////////////////////
//  namespaces

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url                     as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql        as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt        as VoltEngine;
use Phalcon\Mvc\Model\Manager           as ModelManagerAdapter;
use Phalcon\Mvc\Model\Metadata\Memory   as ModelMetaDataAdapter;
use Phalcon\Session\Adapter\Files       as SessionAdapter;

/////////////////////////////////////////////////////////////////////////////
//  config

$config = new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'mvc_workshop',
        'dbname'      => 'mvc_workshop',
        'password'    => 'HHN(DI@Ec',
    ),
    'application' => array(
        'controllersDir' => '../app/controllers/',
        'modelsDir'      => '../app/models/',
        'viewsDir'       => '../app/views/',
        'cacheDir'       => '../app/cache/',
        'baseUri'        => '/',
    )
));

/////////////////////////////////////////////////////////////////////////////
//  loader

$loader = new \Phalcon\Loader();
$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir
    )
)->register();

/////////////////////////////////////////////////////////////////////////////
//  create dependency injector (di)

$di = new FactoryDefault();

/////////////////////////////////////////////////////////////////////////////
//  router

$di->set('router', $router = new \Phalcon\Mvc\Router(false));
$router->notFound(
    array(
        'controller'    => 'index',
        'action'        => 'notFound'
    )
);

/////////////////////////////////////////////////////////////////////////////
//  url

$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/////////////////////////////////////////////////////////////////////////////
//  models meta

$di->set('modelsManager', function () {
    return new ModelManagerAdapter();
});

$di->set('modelsMetadata', function () {
    return new ModelMetaDataAdapter();
});

/////////////////////////////////////////////////////////////////////////////
//  db connection

$di->set('db', function () use ($config) {
    return new DbAdapter(array(
        'host'      => $config->database->host,
        'username'  => $config->database->username,
        'password'  => $config->database->password,
        'dbname'    => $config->database->dbname
    ));
});

/////////////////////////////////////////////////////////////////////////////
//  session

$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/////////////////////////////////////////////////////////////////////////////
//  view

//  twig includes
require_once "../vendor/Twig/Autoloader.php"; Twig_Autoloader::register();
require_once "../app/classes/Twig.php";
require_once "../app/classes/Twig/Environment.php";
require_once "../app/classes/Twig/CoreExtension.php";
require_once "../app/classes/Twig/Nodes/Assets.php";
require_once "../app/classes/Twig/TokenParsers/Assets.php";

//  twig service
$di->set('twigService', function($view, $di) use ($config) {

    $options    = array('debug'                 => true,
                        'charset'               => 'UTF-8',
                        'base_template_class'   => 'Twig_Template',
                        'strict_variables'      => false,
                        'autoescape'            => false,
                        'cache'                 => false, //'../app/cache/',
                        'auto_reload'           => null,
                        'optimizations'         => -1);
    $arrFolder  = array('../app/views/');
    $twig       = new \Phalcon\Mvc\View\Engine\Twig($view, $di, $options, $arrFolder);

    return $twig;

}, true);

//  view
$di->set('view', function(){

    $view   = new \Phalcon\Mvc\View();
    $view->setViewsDir('../app/views/');
    $view->registerEngines(array(
        '.twig' => 'twigService'
    ));

    return $view;
});