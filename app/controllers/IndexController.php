<?php

class IndexController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
    	//	set view vars
    	$this->view->modelMessage		= Item::findFirst()->name;
		$this->view->controllerMessage	= "Controllers are operational";

		//	set view template
		$this->view->setMainView('index');
    }

    public function notFoundAction()
    {
		//	set view template
		$this->view->setMainView('404');
    }

}

