<?php

/* index.twig */
class __TwigTemplate_547860684b5e1b2ca3549b9ec80b027f5c3e10790ee43bf2b7d5663d44317172 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t<title>MVC Workshop</title>
\t</head>
\t<body>
\t\t";
        // line 7
        if (isset($context["modelMessage"])) { $_modelMessage_ = $context["modelMessage"]; } else { $_modelMessage_ = null; }
        echo $_modelMessage_;
        echo "<br>
\t\tViews are operational<br>
\t\t";
        // line 9
        if (isset($context["controllerMessage"])) { $_controllerMessage_ = $context["controllerMessage"]; } else { $_controllerMessage_ = null; }
        echo $_controllerMessage_;
        echo "
\t</body>
</html>";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 9,  27 => 7,  19 => 1,);
    }
}
